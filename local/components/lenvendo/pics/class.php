<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main,
    Bitrix\Main\Localization\Loc;

class pics extends \CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;
        return $result;
    }

    /*
     * Функция проверки типа страницы: редатироение/просмотр/создание
     */
    protected function getComponentPage()
    {
        $arUrlTemplates = array(
            "view" => "view/#ID#/",
            "add" => "edit/",
            "edit" => "edit/#ID#/",
        );
        $arVariables = array();

        $engine = new CComponentEngine($this);
        $componentPage = $engine->guessComponentPath(
            "/",
            $arUrlTemplates,
            $arVariables
        );

        //если ни один парамерт не подходит, то устанавливаем список
        if(!$componentPage)
        {
            $componentPage = "list";
        }
        $this->arResult['VARIABLES'] = $arVariables;
        return $componentPage;
    }

    public function executeComponent()
    {
        $this->includeComponentTemplate($this->getComponentPage());
    }
}
