<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/edit_page.js");
?>
<div class="pics_edit">
    <div class="draw_area">
        <canvas id="draw_block" width="500px" height="500px">
    </div>
    <?$APPLICATION->IncludeComponent(
        "lenvendo:pics.edit",
        "",
        Array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "AJAX_MODE" => "Y",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "N",
            "AJAX_OPTION_HISTORY" => "N",
        ),
        false
    ); ?>
</div>

<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    ".default",
    array(
        "AREA_FILE_SHOW" => "sect",
        "AREA_FILE_SUFFIX" => "passModal",
        "AREA_FILE_RECURSIVE" => "Y",
        "EDIT_TEMPLATE" => ""
    ),
    false
);?>
