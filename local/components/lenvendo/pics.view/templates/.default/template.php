<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="pics_view">
    <div class="picture_area">
        <img src="<?=$arResult["ITEM"]["PICTURE_PATH"]?>">
    </div>
    <div class="buttons_block">
        <a href="<?=$arResult["ITEM"]["EDIT_PAGE_URL"]?>" type="button" class="btn btn-primary clear">
            <?echo Loc::getMessage("EDIT_BUTTON");?>
        </a>
    </div>
</div>