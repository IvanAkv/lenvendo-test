<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
    Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc;

Loader::includeModule("iblock"); 


class picsView extends \CBitrixComponent
{
    /*
     * Получаем элемент по id
     */
    protected function getItem($id)
    {
        //получаем элемент, используя ORM
        $result = \Bitrix\Iblock\ElementTable::getList(array(
            "select" => array("NAME", "PREVIEW_PICTURE"),
            "filter" => array("ID" => $id, "IBLOCK_ID" => $this->arParams["IBLOCK_ID"]),
        ));
        if ($row = $result->fetch())
        {
            $row["EDIT_PAGE_URL"] = "/edit/".$id."/";
            if ($row["PREVIEW_PICTURE"])
                $row["PICTURE_PATH"] = CFile::GetPath($row["PREVIEW_PICTURE"]);
            $arResult = $row;
        }

        $this->arResult["ITEM"] = $arResult;
    }

    public function executeComponent()
    {
        global $APPLICATION;

        if ($this->arParams["ID"])
        {
            $this->getItem($this->arParams["ID"]);
            $APPLICATION->SetPageProperty("h1", Loc::getMessage("PICTURE_VIEW", array("#ID#" => $this->arParams["ID"])));
            $APPLICATION->AddChainItem(Loc::getMessage("PICTURE_VIEW", array("#ID#" => $this->arParams["ID"])), "/view/".$this->arParams["ID"]."/" );
        }
        if (!$this->arResult["ITEM"])
            LocalRedirect("/");

        $this->includeComponentTemplate();
    }
}
