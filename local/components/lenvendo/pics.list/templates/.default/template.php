<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="controls">
    <a href="/edit/" class="btn btn-success"><?echo Loc::getMessage("ADD_BUTTON");?></a>
</div>
<section class="list_page">
    <div class="row">
        <?foreach ($arResult["ITEMS"] as $arItem){?>
            <article class="col col-md-4 picture_item">
                <div class="caption">
                    <h3><?=$arItem["NAME"]." ".$arItem["ID"]?></h3>
                </div>
                <a href="<?=$arItem["VIEW_PAGE_URL"]?>">
                    <img src="<?=$arItem["PICTURE_PATH"]?>" class="thumbnail">
                </a>
                <div class="picture_controls">
                    <a href="<?=$arItem["VIEW_PAGE_URL"]?>" class="btn btn-success"><?echo Loc::getMessage("SHOW_BUTTON");?></a>
                    <a href="<?=$arItem["EDIT_PAGE_URL"]?>" class="btn btn-primary"><?echo Loc::getMessage("EDIT_BUTTON");?></a>
                </div>
            </article>
        <?}?>
    </div>
</section>
