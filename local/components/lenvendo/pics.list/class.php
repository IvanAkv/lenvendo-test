<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main,
    Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc;

Loader::includeModule("iblock"); 

class picsList extends \CBitrixComponent
{
    /*
     * Получение списка рисунков
     */
    protected function getItems()
    {
        // используем ORM
        $result = \Bitrix\Iblock\ElementTable::getList(array(
            "select" => array("ID", "NAME", "PREVIEW_PICTURE"),
            "filter" => array("IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "ACTIVE" => "Y"),
            'order' => array("ID" => "DESC")
        ));
        while ($row = $result->fetch())
        {
            $row["VIEW_PAGE_URL"] = "/view/".$row["ID"]."/";
            $row["EDIT_PAGE_URL"] = "/edit/".$row["ID"]."/";

            //если нету изображения - не выводим
            if ($row["PREVIEW_PICTURE"])
            {
                $row["PICTURE_PATH"] = CFile::GetPath($row["PREVIEW_PICTURE"]);
                $arResult[] = $row;
            }
        }
        $this->arResult["ITEMS"] = $arResult;
    }

    public function executeComponent()
    {
        $this->getItems();
        $this->includeComponentTemplate();
    }
}
