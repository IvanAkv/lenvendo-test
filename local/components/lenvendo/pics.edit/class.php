<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Context;

Loader::includeModule("iblock"); 
Loc::loadMessages(__FILE__);

class picsEdit extends \CBitrixComponent
{
    //папака для временного хранения рисунков
    protected $pict_dir = "/upload/";

    public function onPrepareComponentParams($arParams)
    {
        $result = $arParams;
        return $result;
    }

    protected function getItem($id)
    {
        //получаем элемент, используя ORM
        $result = \Bitrix\Iblock\ElementTable::getList(array(
            "select" => array("NAME", "PREVIEW_PICTURE"),
            "filter" => array("ID" => $id, "IBLOCK_ID" => $this->arParams["IBLOCK_ID"]),
        ));
        if ($row = $result->fetch())
        {
            if ($row["PREVIEW_PICTURE"])
                $row["PICTURE_PATH"] = CFile::GetPath($row["PREVIEW_PICTURE"]);

            //получаем пароль из свойста через старый апи(
            $res = CIBlockElement::GetProperty($this->arParams["IBLOCK_ID"], $id, array(), array("CODE" => "PASSWORD"));
            if ($ob = $res->GetNext())
            {
                  $row["PASSWORD"] = $ob['VALUE'];   
            }
            $arResult = $row;   
        }

        $this->arResult["ITEM"] = $arResult;
    }

    protected function getRequest(){
        $this->arResult['REQUEST'] = Context::getCurrent()->getRequest();
    }

    /**
     * проверяем пароль
     * для новых изображений проверяем его наличие и длину
     * для существующих проверяем соответствие пароля
     */
    protected function checkPassword()
    {
        $arRequest = $this->arResult["REQUEST"];

        //проверка наличия ID элемента. Если ID есть, значит происходит редактирование изображения
        if($this->arParams["ID"]) {

            //проверка пароля
            if ($this->arResult["ITEM"]["PASSWORD"] != $arRequest["PASSWORD"])
            {
                $this->arResult["ERROR_TEXT"] = Loc::getMessage("INCORRECT_PASSWORD");
                $this->arResult["ACCESS"] = "N";
                return false;
            }

        }else{

            if (empty($arRequest["PASSWORD"]))
            {
                $this->arResult["ERROR_TEXT"] = Loc::getMessage("ENTER_PASSWORD");
                return false;
            }

            if (strlen($arRequest["PASSWORD"]) < 6)
            {
                $this->arResult["ERROR_TEXT"] = Loc::getMessage("SMALL_PASSWORD");
                return false;
            }
        }

        $this->arResult["ACCESS"] = "Y";
        return true;
    }

    /**
     * Создание файла рискунка и подготовка массива
     */
    protected function uploadPicture($picture){
        //удаляем лишние для создания файла png
        $image_parts = explode(";base64,", $picture);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $this->pict_dir . uniqid() . '.png';
        //создание файла
        file_put_contents($_SERVER["DOCUMENT_ROOT"].$file, $image_base64);
        $arResult = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$file);
        return $arResult;
    }

    /**
     * Сохранение рисунка
     */
    protected function savePicture(){
        $arRequest = $this->arResult["REQUEST"];

        $pict = $this->uploadPicture($arRequest["PICTURE"]);
        $this->arResult["SAVE_PICTURE"] = "Y";
        $el = new CIBlockElement;

        //Если рисунок уже существует то обновляем
        if($this->arParams["ID"])
        {
            $arLoadProductArray = Array(
                "PREVIEW_PICTURE" => $pict,
            );
            if($result = $el->Update($this->arParams["ID"], $arLoadProductArray))
            {
                $this->arResult["SAVE_STATUS"] = Loc::getMessage("SUCCESS_UPDATED");
            } else {
                $this->arResult["ERROR_TEXT"] = 'Error: '.$el->LAST_ERROR;
            }
        }else{
            $PROP["PASSWORD"] = $arRequest["PASSWORD"];
            $arLoadProductArray = Array(
                'PROPERTY_VALUES' => $PROP,
                "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                "PREVIEW_PICTURE" => $pict,
                "NAME" => "Рисунок"
            );
            if($pict_id = $el->Add($arLoadProductArray))
            {
                $this->arResult["SAVE_STATUS"] = Loc::getMessage("SUCCESS_SAVED");
                $this->arResult["REDIRECT_URL"] = "/";
            } else {
                $this->arResult["ERROR_TEXT"] = 'Error: '.$el->LAST_ERROR;
            }
        }
    }

    public function executeComponent()
    {
        global $APPLICATION;
        $this->getRequest();
        $this->ajax_call = $this->arResult["REQUEST"]["AJAX_CALL"];
        $chain_url = "/edit/";
        //проврка на тип действия. Создание рисунка или редактирование
        if ($this->arParams["ID"])
        {
            //устанавливем тип дествия. Используется как класс для кнопки сохранения
            $this->arResult["ACTION_TYPE"] = "edit";
            $this->getItem($this->arParams["ID"]);
            $title = Loc::getMessage("PICTURE_EDIT", array("#ID#" => $this->arParams["ID"]));
            $chain_url .= $this->arParams["ID"]."/";
        }else{
            $this->arResult["ACCESS"] = "Y";
            $this->arResult["ACTION_TYPE"] = "add";
            $title = Loc::getMessage("PICTURE_ADD");
        }
        //заголовок и ссылка в навигацию
        $APPLICATION->SetPageProperty("h1", $title);
        $APPLICATION->AddChainItem($title, $chain_url);

        if ($this->ajax_call == "Y")
        {
            //сначала проверяем пароль
            if($this->checkPassword())
            {
                //если был вызван запрос на сохранение рисунка
                if ($this->arResult["REQUEST"]["ACTION"] == "SAVE")
                {
                    $this->savePicture();
                }
            }
        }

        $this->includeComponentTemplate();
    }
}
