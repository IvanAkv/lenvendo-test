<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<?if($arResult["ACCESS"] == "Y"){?>
    <form action="<?=$APPLICATION->GetCurPage();?>" method="POST" id="EDIT_FORM">
        <input type="hidden" value="SAVE" name="ACTION">
        <input type="hidden" value="<?=$arResult["ID"]?>" name="ID">
        <input type="hidden" value="<?=$arResult["REQUEST"]["PASSWORD"]?>" name="PASSWORD">
        <input type="hidden" value="" name="PICTURE">
        <div class="buttons_block">
            <button type="button" class="btn btn-danger clear"><?echo Loc::getMessage("ERASE_BUTTON");?></button>
            <button type="button" class="btn btn-success <?=$arResult["ACTION_TYPE"]?>"><?echo Loc::getMessage("SAVE_BUTTON");?></button>
        </div>
        <input type="submit" value="Y">
        <div class="error_text"><?=$arResult["ERROR_TEXT"]?></div>
        <div class="save_text"><?=$arResult["SAVE_STATUS"]?></div>
    </form>
<?}else{?>
    <div class="access_block">
        <form class="form-horizontal" action="<?=$APPLICATION->GetCurPage();?>" method="POST" >
            <input type="hidden" value="GET_ACCESS" name="ACTION">
            <div class="modal-header">
                <h4 class="modal-title"><?echo Loc::getMessage("PASSWORD_TITLE");?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="inputPassword" class="control-label col-xs-2"><?echo Loc::getMessage("PASSWORD");?></label>
                    <div class="col-xs-10">
                        <input type="password" class="form-control" name="PASSWORD">
                    </div>
                </div>
                <div class="form-error">
                    <?=$arResult["ERROR_TEXT"]?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary save"><?echo Loc::getMessage("COMFIRM_BUTTON");?></button>
            </div>
        </form>
    </div>
<?}?>
<?if ($arResult["ITEM"]["PICTURE_PATH"]){?>
    <script type="text/javascript">
        var pictData = "<?=$arResult["ITEM"]["PICTURE_PATH"]?>";
    </script>
<?}?>
<?if ($arResult["REDIRECT_URL"]){?>
    <script>
        location.href = "<?=$arResult["REDIRECT_URL"]?>";
    </script>
<?}?>