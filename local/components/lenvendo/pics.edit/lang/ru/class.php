<?
$MESS["INCORRECT_PASSWORD"] = "Неверный пароль";
$MESS["ENTER_PASSWORD"] = "Введите пароль";
$MESS["SMALL_PASSWORD"] = "Пароль должен быть не менее 6 символов";
$MESS["SUCCESS_UPDATED"] = "Успешно обновлено";
$MESS["SUCCESS_SAVED"] = "Успешно сохранено";
$MESS["PICTURE_EDIT"] = "Редактирование рисунка #ID#";
$MESS["PICTURE_ADD"] = "Создание нового рисунка";
?>