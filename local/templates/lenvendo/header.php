<!DOCTYPE html>
<?
use Bitrix\Main\Page\Asset;
?>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='width=device-width,initial-scale=1'/>
    <link rel="shortcut icon" href="/favicon.png" type="image/png">
    <title><?$APPLICATION->ShowTitle()?></title>
    <?
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/styles_main.css");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/script.js");
    ?>
    <?$APPLICATION->ShowHead()?>
</head>

<body>
    <?$APPLICATION->ShowPanel();?>

    <header class="main_header">
        <div class="wrapper container">
            <div class="logo">
                <?
                $APPLICATION->IncludeFile("/inc/top_text.php", Array(), Array(
                    "MODE"      => "html",
                    "NAME"      => "Текст",      // текст всплывающей подсказки на иконке
                ));
                ?>
            </div>
        </div>
    </header>


    <main role="main">
        <div class="wrapper container">
            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "lenvendo", Array(
                "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
                    "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                    "SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                ),
                false
            );?>
            <h1><?$APPLICATION->ShowProperty("h1");?></h1>

