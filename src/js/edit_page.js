(function() {
    window.edit_page = window.edit_page || {};
    var sketcher;

    window.edit_page = {
        // иницилизация области рисования
        initSketcher: function () {
            //если есть рисунок, то добавляем на область
             if (typeof pictData !== 'undefined') {
                var canvas = document.getElementById("draw_block");
                var ctx = canvas.getContext("2d");


                var image = new Image();
                image.onload = function() {
                    ctx.drawImage(image, 0, 0);
                };
                image.src = pictData;
            }
            sketcher = atrament('#draw_block');
        },
        // обработка нажатий кнопок
        initButtons: function () {
            $("body").on("click", ".buttons_block .clear", function () {
                sketcher = atrament('#draw_block');
                sketcher.clear();
            });

            $("body").on("click", ".buttons_block .add", function () {
                edit_page.$modal.modal('show');
            });

            $("body").on("click", ".buttons_block .edit", function () {
                edit_page.savePicture();
            });

            //клик в модальном окне по кнопке Сохранить
            $("body").on("click", "#passModal .save", function () {
                pass = edit_page.$modal.find("#inputPassword");
                //переносим пароль с модальной формы в форму изображения
                $("#EDIT_FORM").find("input[name='PASSWORD']").val(pass.val());
                $("#passModal").modal('hide');
                window.edit_page.savePicture();
            });
        },
        //функция сохранения изображения
        savePicture: function () {
            form = $("#EDIT_FORM");
            //преобразуем изображение с cavans
            var dataURL = sketcher.toImage();
            form.find("input[name='PICTURE']").val(dataURL);
            //отправляем запрос ajax на сохранение
            form.find("input[type='submit']").click();
        },
        init: function() {
            this.$modal = $("#passModal");
            this.initSketcher();
            this.initButtons();
        },
    };

    $(document).ready(function () {
        edit_page.init();
    });
})();