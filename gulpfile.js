'use strict';

// init
var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    plumber     = require('gulp-plumber'), 
    uglify      = require('gulp-uglify'),
    rigger      = require('gulp-rigger'), 
    cleanCSS    = require('gulp-clean-css'),
    watch       = require('gulp-watch');


// write routs
var path = {
    build: {
        js:            'local/templates/lenvendo/js/',
        styles:        'local/templates/lenvendo/',
    },
    src: {
        js:                'src/js/*.*',
        styles:            'src/styles/*.*',
        stylesPartials:    'src/styles/partials/',
    },
    watch: {
        js:        'src/js/**/*.js',
        styles:    'src/styles/**/*.scss',
    }
};

// javascript
gulp.task('js:build', function () {
    gulp.src(path.src.js)               // Найдем наш main файл
        .pipe(plumber())
        .pipe(rigger())
        .pipe(uglify())                 // Сожмем наш js
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.js)) // Выплюнем готовый файл в build
});


gulp.task('styles:build', function () {
    gulp.src(path.src.styles)               // Выберем наш main.scss
        .pipe(plumber())
        .pipe(sass())
        .pipe(cleanCSS())
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.styles)) // И в build
});

gulp.task('build', [
    'js:build',
    'styles:build',
]);

gulp.task('watch', function(){
    watch([path.watch.js], function(event, cb) {
        setTimeout(function(){
          gulp.start('js:build');
        }, 1000);
    });
    watch([path.watch.styles], function(event, cb) {
        setTimeout(function(){
          gulp.start('styles:build');
        }, 1000);
    });
});

gulp.task('default', ['build', 'watch']);